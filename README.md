## Get Started

Create a folder: `/data` in root directory of a project.

Then download .zip file from [Google drive](https://drive.google.com/drive/folders/1YPKvDLdwMav5fGc5kdeRuv3dxRBhDyGT?usp=sharing) and extract into created folder.

### Build

To start a project, run `docker build --build-arg <build arguments> -t <user-name>/<image-name>:<tag-name> /path/to/Dockerfile`.

In my case: `docker build --build-arg APP_DIR=var/app -t barsky/stream-data-processing:V1 .`

### Launch

After building the image, run `docker run -p <External-port:exposed-port> -d --name <name of the container> <user-name>/<image-name>:<tag-name>`.

In my case: `docker run -p 8000:3000 -d --name stream-data-processing barsky/stream-data-processing:V1`

### Start processing

Now, server is already running, but doesn`t do anything. To make it do actual work/process, run ``curl -i localhost:8000``. It will send get request and launch the processing.
