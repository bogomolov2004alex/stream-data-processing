import AgeOutlierRemovalModel from './model.js';

const deferred_chunks = [];
let isProcessing = false;
let processed_chunks_count = 0;
let timer;

process.on('message', m => {
	if (m.close) {
		console.log(
			`\nTotal number of chunks processed: ${processed_chunks_count}`,
			`\nChild process ${process.pid} received 'close' message. Exiting...`
		);
		process.exit(0);
	}

	if (isProcessing) {
		deferred_chunks.push(...m.data);
	} else {
		const chunks_to_process = [...deferred_chunks, ...m.data];

		clearTimeout(timer);

		timer = setTimeout(() => {
			console.log(
				`Child process ${process.pid} processed ${chunks_to_process.length} chunks.`
			);

			const processed_chunks =
				AgeOutlierRemovalModel.getObjectWithRemovedOutliers(chunks_to_process);

			process.send(processed_chunks);

			processed_chunks_count += processed_chunks.length;
			isProcessing = false;
		}, 2000); // 2 seconds

		deferred_chunks.length = 0;
		isProcessing = true;
	}
});
