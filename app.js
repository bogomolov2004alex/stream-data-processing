import { fork } from 'child_process';
import express from 'express';
import {
	CHILD_PROCESS_COUNT,
	FILE_NAME,
	PORT,
	RECORD_COUNT,
	STATIC_PATH,
	getFullPath,
} from './config.js';
import MyDuplex from './duplex.js';

const main = () => {
	const child_processes = [];
	const duplex = new MyDuplex(STATIC_PATH, child_processes);

	for (let i = 0; i < CHILD_PROCESS_COUNT; i++) {
		const childProcess = fork(getFullPath('subprocess.js'));

		childProcess.on('message', m => {
			console.log(`Processed chunks: ${m.length}`);
			duplex._write_yml(FILE_NAME, m);
		});

		child_processes.push(childProcess);
	}

	console.log(
		child_processes.forEach(childProcess =>
			console.log('Process created:', childProcess.pid)
		)
	);

	duplex._read_csv(FILE_NAME, RECORD_COUNT);
};

const app = express();

app.get('/', (req, res) => {
	res.sendStatus(200);
	main();
});

app.listen(PORT, err => {
	if (err) return console.error(err);
	console.log(`Server is running on port ${PORT}`);
});
