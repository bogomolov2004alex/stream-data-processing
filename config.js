import dotenv from 'dotenv';
import path from 'path';
import { fileURLToPath } from 'url';

/* Static values configuration */
dotenv.config();

export const FILE_NAME = process.env.FILE_NAME;
export const CHILD_PROCESS_COUNT = parseInt(process.env.CHILD_PROCESS_COUNT);
export const RECORD_COUNT = parseInt(process.env.RECORD_COUNT);

export const PORT = process.env.PORT || 3000;

/* Path configuration */
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

export const STATIC_PATH = path.resolve(__dirname, 'data');

export const getFullPath = file_name => path.resolve(__dirname, file_name);
