import csv from 'csv-parser';
import fs from 'fs';
import yaml from 'js-yaml';

class MyDuplex {
	constructor(static_path, child_processes) {
		this.static_path = static_path;
		this.child_processes = child_processes;
		this.chunks = [];
		this.chunk_count = 0;
	}

	getFullFilePath(file_name, postfix) {
		return `${this.static_path}/${file_name}.${postfix}`;
	}

	sendMessageToProcess(chunks) {
		const random_index = Math.floor(
			Math.random() * this.child_processes.length
		);

		const message = {
			data: chunks,
		};

		this.child_processes[random_index].send(message);
	}

	_read_csv(file_name, record_count) {
		fs.createReadStream(this.getFullFilePath(file_name, 'csv'), 'utf8')
			.pipe(csv())
			.on('data', chunk => {
				this.chunks.push(chunk);
				this.chunk_count++;

				if (this.chunks.length === record_count) {
					this.sendMessageToProcess(this.chunks);
					this.chunks = [];
				}
			})
			.on('end', () => {
				if (this.chunks.length > 0) {
					console.log('Chunks left:', this.chunks.length);
					this.sendMessageToProcess(this.chunks);
				}

				console.log('Total number of chunks read:', this.chunk_count);
				this.child_processes.forEach(child_process => {
					child_process.send({ close: true });
				});

				console.log('end');
			});
	}

	_write_yml(file_name, chunks) {
		chunks.forEach(chunk => {
			fs.appendFileSync(
				this.getFullFilePath(file_name, 'yml'),
				yaml.dump(chunk),
				'utf8'
			);
		});
	}
}

export default MyDuplex;
